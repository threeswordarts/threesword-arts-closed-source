using UnityEngine;
using System.Collections;
using System;

public class pauseMenu : MonoBehaviour {

	private int buttonWidth = 200;
	private int buttonHeight = 50;
	private int groupWidth = 200;
	private int groupHeight = 200;
	public string[] credits= {
		"A ThreeSword Arts Production",
		"Programming by ThreeSword Arts IT",
		"Logo Made By ThreeSword Arts",
		"Made By ThreeSword Arts"} ;

	bool paused = false;

	void Start ()
	{
		Screen.lockCursor = true;
		Time.timeScale = 1;
		Screen.lockCursor = false;
		Time.timeScale = 0;
		paused = togglePause();
	}

	void OnGUI ()
	{
		if(paused)
		{
			GUI.BeginGroup(new Rect(((Screen.width/2) - (groupWidth/2)),((Screen.height/2) - (groupHeight/2)), groupWidth, groupHeight));
			if(GUI.Button (new Rect(0,0,buttonWidth,buttonHeight),"Continue"))
			{
				Debug.Log("Continuing!" + "\n" + System.DateTime.Now.ToString());
				paused = togglePause();
			}
			if(GUI.Button(new Rect(0,75,buttonWidth,buttonHeight),"Restart Game"))
			{
				Debug.Log("Restarting Game!" + "\n" + System.DateTime.Now.ToString());
				Application.LoadLevel("scene1");
			}
			if(GUI.Button(new Rect(0,150,buttonWidth,buttonHeight),"Quit Game"))
			{
				Debug.Log("Closing Game!" + "\n" + System.DateTime.Now.ToString());
				Application.Quit();
			}
			GUI.EndGroup();
		}
	}
	

	void Update ()
	{
		if(Input.GetKeyUp(KeyCode.Escape))
			paused = togglePause();
	}

	bool togglePause()
	{
		if(Time.timeScale == 0)
		{
			GameObject.Find("First Person Controller").GetComponent<MouseLook>().enabled = true;
			GameObject.Find("1st Person").GetComponent<MouseLook>().enabled = true;
			GameObject.Find("3rd Person").GetComponent<MouseLook>().enabled = false;
			GameObject.Find("Top View").GetComponent<MouseLook>().enabled = false;
//			GameObject.Find("Ball Cam").GetComponent<MouseLook>().enabled = false;
//			GameObject.Find("Overview Cam").GetComponent<MouseLook>().enabled = false;
			Debug.Log("Unpaused" + "\n" + System.DateTime.Now.ToString());
			Screen.lockCursor = true;
			Time.timeScale = 1;
			return(false);
		}
		else
		{
			GameObject.Find("First Person Controller").GetComponent<MouseLook>().enabled = false;
			GameObject.Find("3rd Person").GetComponent<MouseLook>().enabled = false;
			GameObject.Find("1st Person").GetComponent<MouseLook>().enabled = false;
			GameObject.Find("Top View").GetComponent<MouseLook>().enabled = false;
//			GameObject.Find("Ball Cam").GetComponent<MouseLook>().enabled = false;
//			GameObject.Find("Overview Cam").GetComponent<MouseLook>().enabled = false;
			Debug.Log("Paused" + "\n" + System.DateTime.Now.ToString());
			Screen.lockCursor = false;
			Time.timeScale = 0;
			return(true);
		}
	}
}