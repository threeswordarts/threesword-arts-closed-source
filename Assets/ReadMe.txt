Scaffolding v1.0
http://www.scaffoldingunity.com/
Created by Jon Reid

——————————————————————————————————————————————————————————————————————————————————————————

This package comes with a sample project to help you get going. The only folder that you need for your project is the Scaffolding folder. All the rest are project specific and can be deleted.

For instructions on how to use Scaffolding, please see the how to video located here:
http://www.scaffoldingunity.com/quickguides/quickstart/

Accompanying documentation can be found on the Scaffolding site:
http://www.scaffoldingunity.com/

The included font: Yanone Kaffeesatz is distrubuted under the Open Font License, and is included within Scaffolding for demonstration purposes.
http://www.yanone.de/typedesign/kaffeesatz/
http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL

——————————————————————————————————————————————————————————————————————————————————————————
Change log:
v1.0 - initial release.
- No changes, or all the changes.